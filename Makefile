all: build copy

build:
	npm run-script build

copy:
	cp dist/eztz.min.js ../dune-wallet/skin/js

install:
	npm install
